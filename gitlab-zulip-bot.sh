#!/bin/bash

usage='
Send a message to the zulipchat, stream CI-falue
where the topic is set to the name of the git repo 
it is executed from. This is intended to run as part of 
gitlab ci.


Simply add:

zulip-report-failure:
  stage: report
  script:
    - apk add -q curl
    - sh scripts/zulip-bot.sh
  when: on_failure
  only:
    - master
    - tags
    - schedules

to the .gitlab-ci.yml
'

MSG=$(cat <<-END
@**all**

## $CI_COMMIT_REF_NAME just failed!!!**

The pipeline was: $CI_PIPELINE_URL


It was the commit: 

> $CI_COMMIT_MESSAGE 

that failed
END
)

curl -X POST https://desupervised.zulipchat.com/api/v1/messages \
    -u "$CI_BOT_EMAIL":$CI_BOT_TOKEN\
    -d "type=stream" \
    -d "to=CI-failure" \
    -d "subject="$CI_PROJECT_NAME \
    -d "content=$MSG"


